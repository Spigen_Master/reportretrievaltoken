from selenium import webdriver
import time
import json
from pymongo import MongoClient
from datetime import datetime, timedelta
import sys
import urllib3
import requests
import xmltodict
import io
from bs4 import BeautifulSoup
urllib3.disable_warnings()
reload(sys)
sys.setdefaultencoding('UTF-8')


# Using Chrome to access web

def retrieveInitialToken(company):
    connectionUri = "mongodb://SP_DBA_SA_ACCOUNT:!Q%40W#E$R%25T6y7u8i9o0p@204.236.191.249:39017/?authSource=admin"
    client = MongoClient(connectionUri)
    access_key_db = client.AMZSPIDER_ALL_ACCESS_KEY_INC
    keys = access_key_db.AMZSPIDER_ALL_ACCESS_KEY_INC
    query = {"COMPANYNAME" : company}
    token = keys.find_one(query)
    return token['REFRESHTOKEN']

def get_token(token):
    url = "https://api.amazon.com/auth/o2/token"

    payload = 'grant_type=refresh_token&refresh_token=' + token + '&client_id=amzn1.application-oa2-client.1608f61ccfd345a7a6480b3ce5b89ce8&client_secret=78b9c1ef786a56f5c52e9faf2ccdee6faf5b6c43f1eaccf6c13859315e2c87f7&scope=sellingpartnerapi%3A%3Amigration'
    headers = {
        'host': 'api.amazon.com',
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    res = json.loads(response.text)
    return res["access_token"]


def generate_date(time_delta):
    yesterday = datetime.now() - timedelta(days=time_delta)
    yesterday.strftime('%m-%d-%Y')
    if yesterday.month < 10:
        yesterday_month = '0' + str(yesterday.month)
    else:
        yesterday_month = str(yesterday.month)
    if yesterday.day < 10:
        yesterday_day = '0' + str(yesterday.day)
    else:
        yesterday_day = str(yesterday.day)

    return yesterday_day, yesterday_month, yesterday.year

def runDriverInvt(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.com"):
    now = datetime.now()

    driver.get('https://sellercentral.amazon.com/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    # #######Check Channel Vendor List and set region#################
    # ReportListURL = "https://sellercentral.amazon.com/trim/component/merchant-marketplace-dropdown"
    # reportListResponse = driver.execute_script(
    #     "var xhr = new XMLHttpRequest();"
    #     + " xhr.open('GET','" + ReportListURL + "', false);"
    #     + "xhr.setRequestHeader('accept', 'application/json, */*');"
    #     + "xhr.setRequestHeader('content-type', 'application/json');"
    #     + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    #
    # soup = BeautifulSoup(reportListResponse)
    # regions = soup.find_all('option')
    #
    # for region in regions:
    #     region_link = region.string.strip() #Region Link is something like www.amazon.com
    #     if region_link == wanted_amazon_region:
    #         regionURL = region.get("value")
    #         break
    #
    # driver.execute_script(
    #     "var xhr = new XMLHttpRequest();"
    #     + " xhr.open('GET','" + regionURL + "', false);"
    #     + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
    #     + "xhr.setRequestHeader('content-type', 'application/json');"
    #     + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # # #######Request Invt Report#################
    print("Requested Order Report")
    InvtRequestURL = "https://sellercentral.amazon.com/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=&reportEndDate=&xdaysBeforeUntilToday=0&startDateTimeOffset=-86400&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2651&language=&disableTimezone=true"
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('POST','" + InvtRequestURL + "', false);"
        +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        +"xhr.setRequestHeader('content-type', 'application/json');"
        +  "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral.amazon.com/reportcentral/api/v1/getDownloadReportStatus?referenceIds="+reportID
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"+"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = json.loads(DownloadRequestResponse)
        status = DownloadRequestResponseDict[0]
        print(status)
        time.sleep(30)
        if status == "Done":
            not_downloaded = False
        elif status == "Fatal":
            raise ValueError

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + "https://sellercentral.amazon.com/reportcentral/api/v1/downloadFile?referenceId="+reportID+"&fileFormat=TSV" + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.quit()

    return filename

def runDriverInvtCA(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.com"):
    now = datetime.now()

    driver.get('https://sellercentral.amazon.ca/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List and set region#################
    ReportListURL = "https://sellercentral.amazon.ca/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    soup = BeautifulSoup(reportListResponse)
    regions = soup.find_all('option')

    for region in regions:
        region_link = region.string.strip() #Region Link is something like www.amazon.com
        if region_link == wanted_amazon_region:
            regionURL = region.get("value")
            break

    driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + regionURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # #######Request Invt Report#################
    print("Requested Order Report")
    InvtRequestURL = "https://sellercentral.amazon.ca/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=&reportEndDate=&xdaysBeforeUntilToday=0&startDateTimeOffset=-86400&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2651&language=&disableTimezone=true"
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('POST','" + InvtRequestURL + "', false);"
        +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        +"xhr.setRequestHeader('content-type', 'application/json');"
        +  "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral.amazon.ca/reportcentral/api/v1/getDownloadReportStatus?referenceIds="+reportID
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"+"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = json.loads(DownloadRequestResponse)
        status = DownloadRequestResponseDict[0]
        print(status)
        time.sleep(30)
        if status == "Done":
            not_downloaded = False
        elif status == "Fatal":
            raise ValueError

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + "https://sellercentral.amazon.ca/reportcentral/api/v1/downloadFile?referenceId="+reportID+"&fileFormat=TSV" + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.close()

    return filename
def runDriverInvtJP(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.jp"):
    now = datetime.now()

    driver.get('https://sellercentral-japan.amazon.com/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    # #######Check Channel Vendor List and set region#################
    # ReportListURL = "https://sellercentral-japan.amazon.com/trim/component/merchant-marketplace-dropdown"
    # reportListResponse = driver.execute_script(
    #     "var xhr = new XMLHttpRequest();"
    #     + " xhr.open('GET','" + ReportListURL + "', false);"
    #     + "xhr.setRequestHeader('accept', 'application/json, */*');"
    #     + "xhr.setRequestHeader('content-type', 'application/json');"
    #     + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    #
    # soup = BeautifulSoup(reportListResponse)
    # regions = soup.find_all('option')
    #
    # for region in regions:
    #     region_link = region.string.strip() #Region Link is something like www.amazon.com
    #     if region_link == wanted_amazon_region:
    #         regionURL = region.get("value")
    #         break
    #
    # driver.execute_script(
    #     "var xhr = new XMLHttpRequest();"
    #     + " xhr.open('GET','" + regionURL + "', false);"
    #     + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
    #     + "xhr.setRequestHeader('content-type', 'application/json');"
    #     + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # #######Request Invt Report#################
    print("Requested Order Report")
    InvtRequestURL = "https://sellercentral-japan.amazon.com/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=&reportEndDate=&xdaysBeforeUntilToday=0&startDateTimeOffset=-86400&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2651&language=&disableTimezone=true"
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('POST','" + InvtRequestURL + "', false);"
        +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        +"xhr.setRequestHeader('content-type', 'application/json');"
        +  "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral-japan.amazon.com/reportcentral/api/v1/getDownloadReportStatus?referenceIds="+reportID
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"+"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = json.loads(DownloadRequestResponse)
        status = DownloadRequestResponseDict[0]
        print(status)
        time.sleep(30)
        if status == "Done":
            not_downloaded = False
        elif status == "Fatal":
            raise ValueError

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + "https://sellercentral-japan.amazon.com/reportcentral/api/v1/downloadFile?referenceId="+reportID+"&fileFormat=TSV" + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.close()

    return filename

def runDriverInvtMX(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.com"):
    now = datetime.now()

    driver.get('https://sellercentral.amazon.com.mx/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List and set region#################
    ReportListURL = "https://sellercentral.amazon.com.mx/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    soup = BeautifulSoup(reportListResponse)
    regions = soup.find_all('option')

    for region in regions:
        region_link = region.string.strip() #Region Link is something like www.amazon.com
        if region_link == wanted_amazon_region:
            regionURL = region.get("value")
            break

    driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + regionURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # #######Request Invt Report#################
    print("Requested Order Report")
    InvtRequestURL = "https://sellercentral.amazon.com.mx/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=&reportEndDate=&xdaysBeforeUntilToday=0&startDateTimeOffset=-86400&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2651&language=&disableTimezone=true"
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('POST','" + InvtRequestURL + "', false);"
        +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        +"xhr.setRequestHeader('content-type', 'application/json');"
        +  "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral.amazon.com.mx/reportcentral/api/v1/getDownloadReportStatus?referenceIds="+reportID
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"+"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = json.loads(DownloadRequestResponse)
        status = DownloadRequestResponseDict[0]
        print(status)
        time.sleep(30)
        if status == "Done":
            not_downloaded = False
        elif status == "Fatal":
            raise ValueError

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + "https://sellercentral.amazon.com.mx/reportcentral/api/v1/downloadFile?referenceId="+reportID+"&fileFormat=TSV" + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.close()

    return filename
def runDriverInvtEU(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.de"):
    now = datetime.now()

    driver.get('https://sellercentral-europe.amazon.com/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List and set region#################
    ReportListURL = "https://sellercentral-europe.amazon.com/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    soup = BeautifulSoup(reportListResponse)
    regions = soup.find_all('option')

    for region in regions:
        region_link = region.string.strip() #Region Link is something like www.amazon.com
        if wanted_amazon_region in region_link:
            regionURL = region.get("value")
            break

    driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + regionURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # #######Request Invt Report#################
    print("Requested Order Report")
    InvtRequestURL = "https://sellercentral-europe.amazon.com/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=&reportEndDate=&xdaysBeforeUntilToday=0&startDateTimeOffset=-86400&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2651&language=&disableTimezone=true"
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('POST','" + InvtRequestURL + "', false);"
        +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        +"xhr.setRequestHeader('content-type', 'application/json');"
        +  "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral-europe.amazon.com/reportcentral/api/v1/getDownloadReportStatus?referenceIds="+reportID
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"+"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = json.loads(DownloadRequestResponse)
        status = DownloadRequestResponseDict[0]
        print(status)
        time.sleep(30)
        if status == "Done":
            not_downloaded = False
        elif status == "Fatal":
            raise ValueError

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + "https://sellercentral-europe.amazon.com/reportcentral/api/v1/downloadFile?referenceId="+reportID+"&fileFormat=TSV" + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.close()

    return filename
def runDriverInvtIN(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.in"):
    now = datetime.now()

    driver.get('https://sellercentral.amazon.in/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List and set region#################
    ReportListURL = "https://sellercentral.amazon.in/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    soup = BeautifulSoup(reportListResponse)
    regions = soup.find_all('option')

    for region in regions:
        region_link = region.string.strip() #Region Link is something like www.amazon.com
        if wanted_amazon_region in region_link:
            regionURL = region.get("value")
            break

    driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + regionURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # # #######Request Invt Report#################
    print("Requested Invt Report")

    InvtRequestURL = "https://sellercentral.amazon.in/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=&reportEndDate=&xdaysBeforeUntilToday=0&startDateTimeOffset=-86400&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2651&language=&disableTimezone=true"
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('POST','" + InvtRequestURL + "', false);"
        +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        +"xhr.setRequestHeader('content-type', 'application/json');"
        +  "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral.amazon.in/reportcentral/api/v1/getDownloadReportStatus?referenceIds="+reportID
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" +"xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"+"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = json.loads(DownloadRequestResponse)
        status = DownloadRequestResponseDict[0]
        print(status)
        time.sleep(30)
        if status == "Done":
            not_downloaded = False
        elif status == "Fatal":
            raise ValueError

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + "https://sellercentral.amazon.in/reportcentral/api/v1/downloadFile?referenceId="+reportID+"&fileFormat=TSV" + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.quit()

    return filename

# Open the website
def runDriver(driver, accesstoken, timedelta, name):
    now = datetime.now()

    driver.get('https://sellercentral.amazon.com/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List#################
    ReportListURL = "https://sellercentral.amazon.com/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # # #######Request Order Report#################
    print("Requested Order Report")
    dateRangeOrdersRequestURL = "https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_xx?recordType=FlatFileAllOrdersReportByOrderDate" \
                                "&reportFileFormat=" \
                                "&eventDateTypeFilterOption=orderDate" \
                                "&eventDateOption=0" \
                                "&fromDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&toDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&startDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&endDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&oneDate=mm%2Fdd%2Fyyyy" \
                                "&fromMonth=12" \
                                "&fromYear=2019" \
                                "&toMonth=12" \
                                "&toYear=2020" \
                                "&startMonth=" \
                                "&startYear=" \
                                "&endMonth=" \
                                "&endYear=" \
                                "&specificMonth=11" \
                                "&specificYear=2020" \
                                "&_=1608151699953"
    #
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + dateRangeOrdersRequestURL + "', false);" + " xhr.setRequestHeader('Content-type', 'text/plain'); " + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = xmltodict.parse(reportRequestResponse)
    reportID = reportRequestResponseDict['response']['reportData']['reportIDAndFileFormat']


    #######Check Report List################# TO-DO use the list if there's an issue with the direct ReportID download Shouldn't be an issue though
    ReportListURL = "https://sellercentral.amazon.com/reportcentral/api/v1/getDownloadHistoryRecords?reportId=2403&isCountrySpecific=false"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    # reportListResponseDict = xmltodict.parse(reportListResponse)

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral.amazon.com/gp/ssof/reports/get-report-status.html/ref=ag_xx_cont_xx?reportIDAndFileFormats="+reportID+"_&_=1608151793489"
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" + " xhr.setRequestHeader('Content-type', 'text/plain'); " + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = xmltodict.parse(DownloadRequestResponse)
        status = DownloadRequestResponseDict['response']['reportData']['displayStatus']
        print(status)
        time.sleep(30)
        if status == "Ready":
            not_downloaded = False
            ActualURL = DownloadRequestResponseDict['response']['reportData']['actualURL']

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ActualURL + "', false);"
        + " xhr.setRequestHeader('Content-type', 'text/plain'); "
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.quit()

    return filename

def runDriverIN(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.in"):
    now = datetime.now()

    driver.get('https://sellercentral.amazon.in/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List#################
    ReportListURL = "https://sellercentral.amazon.in/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # #######Request Order Report#################
    print("Requested Order Report")
    # "https://sellercentral.amazon.in/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV&&reportStartDate=2021%2F01%2F12&reportEndDate=2021%2F01%2F12&xdaysBeforeUntilToday=-1&startDateTimeOffset=0&endDateTimeOffset=0&specialDateOptions=&reportFRPId=2400&language=&disableTimezone=true"
    dateRangeOrdersRequestURL = "https://sellercentral.amazon.in/reportcentral/api/v1/submitDownloadReport?reportFileFormat=TSV" \
                                "&&reportStartDate="+str(year)+"%2F"+month+"%2F"+day+ \
                                "&reportEndDate="+str(year)+"%2F"+month+"%2F"+day+ \
                                "&xdaysBeforeUntilToday=-1" \
                                "&startDateTimeOffset=0" \
                                "&endDateTimeOffset=0" \
                                "&specialDateOptions=" \
                                "&reportFRPId=2400" \
                                "&language=" \
                                "&disableTimezone=true"
    #

    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" +
        " xhr.open('POST','" + dateRangeOrdersRequestURL + "', false);" +
        " xhr.setRequestHeader('accept', 'application/json, */*');" +
        " xhr.setRequestHeader('Content-type', 'application/json'); " +
        "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = json.loads(reportRequestResponse)
    reportID = reportRequestResponseDict['reportReferenceId']


    #######Check Report List################# TO-DO use the list if there's an issue with the direct ReportID download Shouldn't be an issue though
    ReportListURL = "https://sellercentral.amazon.in/reportcentral/api/v1/getDownloadHistoryRecords?reportId=2400&isCountrySpecific=false"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    # reportListResponseDict = xmltodict.parse(reportListResponse)

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral.amazon.in/gp/ssof/reports/get-report-status.html/ref=ag_xx_cont_xx?reportIDAndFileFormats="+reportID+"_&_=1608151793489"
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" + " xhr.setRequestHeader('Content-type', 'application/json, text/plain'); " +"xhr.setRequestHeader('content-type', 'application/json');" + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = xmltodict.parse(DownloadRequestResponse)
        status = DownloadRequestResponseDict['response']['reportData']['displayStatus']
        print(status)
        time.sleep(30)
        if status == "Ready":
            not_downloaded = False
            ActualURL = DownloadRequestResponseDict['response']['reportData']['actualURL']

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ActualURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.quit()

    return filename

def runDriverEU(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.de"):
    now = datetime.now()

    driver.get('https://sellercentral-europe.amazon.com/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    #######Check Channel Vendor List#################
    ReportListURL = "https://sellercentral-europe.amazon.com/trim/component/merchant-marketplace-dropdown"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    soup = BeautifulSoup(reportListResponse)
    regions = soup.find_all('option')

    for region in regions:
        region_link = region.string.strip()  # Region Link is something like www.amazon.com
        if wanted_amazon_region in region_link:
            regionURL = region.get("value")
            break

    driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + regionURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # # #######Request Order Report#################
    print("Requested Order Report")
    dateRangeOrdersRequestURL = "https://sellercentral-europe.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_xx?recordType=FlatFileAllOrdersReportByOrderDate" \
                                "&reportFileFormat=" \
                                "&eventDateTypeFilterOption=orderDate" \
                                "&eventDateOption=0" \
                                "&fromDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&toDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&startDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&endDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&oneDate=mm%2Fdd%2Fyyyy" \
                                "&fromMonth=12" \
                                "&fromYear=2019" \
                                "&toMonth=12" \
                                "&toYear=2020" \
                                "&startMonth=" \
                                "&startYear=" \
                                "&endMonth=" \
                                "&endYear=" \
                                "&specificMonth=11" \
                                "&specificYear=2020" \
                                "&_=1608151699953"
    #
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + dateRangeOrdersRequestURL + "', false);" + " xhr.setRequestHeader('Content-type', 'text/plain'); " + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = xmltodict.parse(reportRequestResponse)
    reportID = reportRequestResponseDict['response']['reportData']['reportIDAndFileFormat']


    #######Check Report List################# TO-DO use the list if there's an issue with the direct ReportID download Shouldn't be an issue though
    ReportListURL = "https://sellercentral-europe.amazon.com/reportcentral/api/v1/getDownloadHistoryRecords?reportId=2403&isCountrySpecific=false"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    # reportListResponseDict = xmltodict.parse(reportListResponse)

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral-europe.amazon.com/gp/ssof/reports/get-report-status.html/ref=ag_xx_cont_xx?reportIDAndFileFormats="+reportID+"_&_=1608151793489"
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" + " xhr.setRequestHeader('Content-type', 'text/plain'); " + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = xmltodict.parse(DownloadRequestResponse)
        status = DownloadRequestResponseDict['response']['reportData']['displayStatus']
        print(status)
        time.sleep(30)
        if status == "Ready":
            not_downloaded = False
            ActualURL = DownloadRequestResponseDict['response']['reportData']['actualURL']

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ActualURL + "', false);"
        + " xhr.setRequestHeader('Content-type', 'text/plain'); "
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.quit()

    return filename

def runDriverJP(driver, accesstoken, timedelta, name, wanted_amazon_region="www.amazon.jp"):
    now = datetime.now()

    driver.get('https://sellercentral-japan.amazon.com/signin')
    time.sleep(2)

    day, month, year = generate_date(timedelta)

    # #######Check Channel Vendor List#################
    # ReportListURL = "https://sellercentral-japan.amazon.com/trim/component/merchant-marketplace-dropdown"
    # reportListResponse = driver.execute_script(
    #     "var xhr = new XMLHttpRequest();"
    #     + " xhr.open('GET','" + ReportListURL + "', false);"
    #     + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
    #     + "xhr.setRequestHeader('content-type', 'application/json');"
    #     + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    #
    # soup = BeautifulSoup(reportListResponse)
    # regions = soup.find_all('option')
    #
    # for region in regions:
    #     region_link = region.string.strip()  # Region Link is something like www.amazon.com
    #     if wanted_amazon_region in region_link:
    #         regionURL = region.get("value")
    #         break
    #
    # driver.execute_script(
    #     "var xhr = new XMLHttpRequest();"
    #     + " xhr.open('GET','" + regionURL + "', false);"
    #     + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
    #     + "xhr.setRequestHeader('content-type', 'application/json');"
    #     + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    # # #######Request Order Report#################
    print("Requested Order Report")
    dateRangeOrdersRequestURL = "https://sellercentral-japan.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_xx?recordType=FlatFileAllOrdersReportByOrderDate" \
                                "&reportFileFormat=" \
                                "&eventDateTypeFilterOption=orderDate" \
                                "&eventDateOption=0" \
                                "&fromDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&toDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&startDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&endDate="+month+"%2F"+day+"%2F"+str(year)+  \
                                "&oneDate=mm%2Fdd%2Fyyyy" \
                                "&fromMonth=12" \
                                "&fromYear=2019" \
                                "&toMonth=12" \
                                "&toYear=2020" \
                                "&startMonth=" \
                                "&startYear=" \
                                "&endMonth=" \
                                "&endYear=" \
                                "&specificMonth=11" \
                                "&specificYear=2020" \
                                "&_=1608151699953"
    #
    reportRequestResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + dateRangeOrdersRequestURL + "', false);" + " xhr.setRequestHeader('Content-type', 'text/plain'); " + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");

    #######Parse Request status#################
    reportRequestResponseDict = xmltodict.parse(reportRequestResponse)
    reportID = reportRequestResponseDict['response']['reportData']['reportIDAndFileFormat']


    #######Check Report List################# TO-DO use the list if there's an issue with the direct ReportID download Shouldn't be an issue though
    ReportListURL = "https://sellercentral-japan.amazon.com/reportcentral/api/v1/getDownloadHistoryRecords?reportId=2403&isCountrySpecific=false"
    reportListResponse = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ReportListURL + "', false);"
        + "xhr.setRequestHeader('accept', 'application/json, text/plain, */*');"
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
    # reportListResponseDict = xmltodict.parse(reportListResponse)

    #######Generate Download Link#################
    print("Retrieving download link")
    time.sleep(60)
    not_downloaded = True
    while not_downloaded:
        DownloadUrl = "https://sellercentral-japan.amazon.com/gp/ssof/reports/get-report-status.html/ref=ag_xx_cont_xx?reportIDAndFileFormats="+reportID+"_&_=1608151793489"
        DownloadRequestResponse = driver.execute_script("var xhr = new XMLHttpRequest();" + " xhr.open('GET','" + DownloadUrl + "', false);" + " xhr.setRequestHeader('Content-type', 'text/plain'); " + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "'); xhr.send('{}'); return xhr.response;");
        DownloadRequestResponseDict = xmltodict.parse(DownloadRequestResponse)
        status = DownloadRequestResponseDict['response']['reportData']['displayStatus']
        print(status)
        time.sleep(30)
        if status == "Ready":
            not_downloaded = False
            ActualURL = DownloadRequestResponseDict['response']['reportData']['actualURL']

    print("Downloading")
    Orders = driver.execute_script(
        "var xhr = new XMLHttpRequest();"
        + " xhr.open('GET','" + ActualURL + "', false);"
        + " xhr.setRequestHeader('Content-type', 'text/plain'); "
        + "xhr.setRequestHeader('content-type', 'application/json');"
        + "xhr.setRequestHeader('x-amz-access-token','" + accesstoken + "');"
        + "xhr.send('{}'); return xhr.response;")

    filename = name + ".txt"
    with io.open(filename, 'w', newline='', encoding='utf8') as target:
            target.write(Orders)

    driver.quit()

    return filename


def main(channel, directory, timedelta, profile):
    print "running driver"
    token = retrieveInitialToken(channel)
    accessToken = get_token(token)
    chrome_options = webdriver.ChromeOptions()
    profile_text = "Profile " + str(profile)
    profile_text_header = "--user-data-dir=C:\Users\Administrator\AppData\Local\Google\Chrome\User Data\\"
    combined_profile = profile_text_header + profile_text
    chrome_options.add_argument(combined_profile)
    driver = webdriver.Chrome(chrome_options=chrome_options)
    not_downloaded = True
    fatal_counter = 0
    while not_downloaded:
        try:
            if channel == "SPIGEN_DE":
                filename = runDriverEU(driver, accessToken, timedelta, directory + channel, 'www.amazon.de')
            elif channel == "SPIGEN_UK":
                filename = runDriverEU(driver, accessToken, timedelta, directory + channel, 'www.amazon.co.uk')
            elif channel == "SPIGEN_IN":
                filename = runDriverIN(driver, accessToken, timedelta, directory + channel, 'www.amazon.in')
            elif channel == "CIEL_JP":
                filename = runDriverJP(driver, accessToken, timedelta, directory + channel, 'www.amazon.jp')
            else:
                filename = runDriver(driver, accessToken, timedelta, directory + channel)
            not_downloaded = False
        except ValueError:
            print("Fatal error countered")
            fatal_counter += 1
            if fatal_counter > 5:
                print("fatal errors detected over 5 times")
                driver.close()
                raise KeyError
            time.sleep(10)
    return filename

def invt_main(channel, directory, timedelta, profile, wanted_amazon_region="www.amazon.com"):
    print "running driver"
    token = retrieveInitialToken(channel)
    accessToken = get_token(token)
    chrome_options = webdriver.ChromeOptions()
    profile_text = "Profile " + str(profile)
    profile_text_header = "--user-data-dir=C:\Users\Administrator\AppData\Local\Google\Chrome\User Data\\"
    combined_profile = profile_text_header + profile_text
    chrome_options.add_argument(combined_profile)
    driver = webdriver.Chrome(chrome_options=chrome_options)
    not_downloaded = True
    fatal_counter = 0
    while not_downloaded:
        try:
            if channel == "SPIGEN_CA":
                filename = runDriverInvtCA(driver, accessToken, timedelta, directory + channel,'www.amazon.ca')
            elif channel == "SPIGEN_MX":
                filename = runDriverInvtMX(driver, accessToken, timedelta, directory+channel, 'www.amazon.com.mx')
            elif channel == "CSO_CA":
                filename = runDriverInvtCA(driver, accessToken, timedelta, directory+channel, 'www.amazon.ca')
            elif channel == "SPIGEN_DE" and wanted_amazon_region == 'www.amazon.co.uk':
                filename = runDriverInvtEU(driver, accessToken, timedelta, directory + channel, 'www.amazon.co.uk')
            elif channel == "SPIGEN_DE":
                filename = runDriverInvtEU(driver, accessToken, timedelta, directory + channel, 'www.amazon.de')
            elif channel == "SPIGEN_UK":
                filename = runDriverInvtEU(driver, accessToken, timedelta, directory + channel, 'www.amazon.co.uk')
            elif channel == "SPIGEN_IN":
                filename = runDriverInvtIN(driver, accessToken, timedelta, directory + channel, 'www.amazon.in')
            elif channel == "CIEL_JP":
                filename = runDriverInvtJP(driver, accessToken, timedelta, directory + channel, 'www.amazon.jp')
            else:
                filename = runDriverInvt(driver, accessToken, timedelta, directory + channel, wanted_amazon_region)
            not_downloaded = False
        except ValueError:
            print("Fatal error countered")
            fatal_counter += 1
            if fatal_counter > 5:
                print("fatal errors detected over 5 times")
                driver.close()
                raise KeyError
            time.sleep(10)
    return filename

# invt_main("SPIGEN_CA", "C:\Users\Administrator\Desktop\ReportRetrievalToken\\", 1, 100)